FROM debian:latest



RUN apt-get update -qq \
    && apt-get install -y \
    curl \
    bind9 \
    expect \
    expect-dev \
    libnet-ssleay-perl \
    && mkdir /usr/share/webmin \
    && /usr/bin/perl -e install Time::Local \
    && curl -o /tmp/webmin.tar.gz https://superb-dca2.dl.sourceforge.net/project/webadmin/webmin/1.870/webmin-1.870-minimal.tar.gz \
    && tar -xvf /tmp/webmin.tar.gz -C /usr/share/webmin --strip-components=1 \
    && curl -o /tmp/bind8.wbm.gz https://download.webmin.com/download/modules/bind8.wbm.gz  

COPY install.sh /tmp/install.sh
COPY install.exp /tmp/install.exp

EXPOSE 10000/tcp 53
VOLUME /etc

ENTRYPOINT [ "sh", "/tmp/install.sh" ]